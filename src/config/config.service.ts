import {config, DotenvParseOutput} from 'dotenv'

import {IConfigService} from './config.interface'

export class ConfigService implements IConfigService {
    private readonly config: DotenvParseOutput
    constructor() {
        const {error, parsed} = config()

        if(error){
            throw new Error('No .env')
        }
        if(!parsed){
            throw new Error('No parsed')
        }

        this.config = parsed
    }
    get(key: string): string {
        const res = this.config[key]

        if(!res){
            throw new Error('No key')
        }

        return res
    }
}