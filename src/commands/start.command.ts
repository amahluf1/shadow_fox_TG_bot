import {Command} from './command.class'
import {Markup, Telegraf} from 'telegraf'
import {IBotContext} from '../context/context.interface'

export class StartCommand extends Command {
    constructor(bot: Telegraf<IBotContext>) {
        super(bot)
    }
    handle(): void {
        this.bot.start(ctx => {
            console.log(ctx)
            ctx.reply('Hello',Markup.inlineKeyboard([
                Markup.button.callback('👍', 'like'),
                Markup.button.callback('👎', 'dislike'),
            ]))
        })

        this.bot.action('like', ctx => {
            ctx.session.courseLike = true
            ctx.editMessageText('Cool! :)')
        })
        this.bot.action('dislike', ctx => {
            ctx.session.courseLike = true
            ctx.editMessageText('No cool! :(')
        })
    }
}